import React from "react";

const IntranetCard = (props) => {
  return <div className="intranet-card">{props.title}</div>;
};

export default IntranetCard;

import React from "react";
import Button from "@mui/material/Button";
import DeleteIcon from "@mui/icons-material/Delete";
import { styled } from "@mui/material/styles";
import RefreshIcon from "@mui/icons-material/Refresh";

const BootstrapButton = styled(Button)({
  boxShadow: "none",
  textTransform: "none",
  fontSize: 16,
  padding: "6px 12px",
  border: "1px solid",
  marginBottom: "10px",
  lineHeight: 1.5,
  backgroundColor: "#FF0000", // Rouge
  borderColor: "#CC0000", // Rouge plus sombre
  fontFamily: [
    "-apple-system",
    "BlinkMacSystemFont",
    '"Segoe UI"',
    "Roboto",
    '"Helvetica Neue"',
    "Arial",
    "sans-serif",
    "Apple Color Emoji",
    "Segoe UI Emoji",
    "Segoe UI Symbol",
  ].join(","),
  "&:hover": {
    backgroundColor: "#FF3333", // Rouge plus clair au survol
    borderColor: "#CC3333", // Rouge plus sombre au survol
    boxShadow: "none",
  },
  "&:active": {
    boxShadow: "none",
    backgroundColor: "#CC0000", // Rouge plus sombre au clic
    borderColor: "#990000", // Rouge encore plus sombre au clic
  },
  "&:focus": {
    boxShadow: "0 0 0 0.2rem #CC0000", // Rouge plus sombre au focus
  },
});

const RefreshButton = (props) => {
  return (
    <BootstrapButton
      variant="contained"
      endIcon={<RefreshIcon />}
      onClick={props.function}
    >
      Refresh
    </BootstrapButton>
  );
};

export default RefreshButton;

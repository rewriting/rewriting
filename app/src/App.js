// src/App.js
import React from "react";
import { Routes, Route } from "react-router-dom";
import MaintenancePage from "./pages/MaintenancePage";
import { UserContextProvider } from "./UserContext";

const App = () => {
    return ( <
        UserContextProvider >
        <
        Routes >
        <
        Route path = "*"
        element = { < MaintenancePage / > }
        /> < /
        Routes > <
        /UserContextProvider>
    );
};

export default App;
import React, { useEffect } from "react";
import Title from "../../../components/Title";
import Section from "../../../components/Section";
import Path from "../../../components/Path";
import Footer from "../../../components/Footer";
import NavBar from "../../../components/temp/NavBar";

const ExternalRessourcesPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  const sections = [
    {
      title: "Section",
      content: ["Text"],
    },
    {
      title: "Section",
      content: ["Text"],
    },
    {
      title: "Section",
      content: ["Text"],
    },
    {
      title: "Section",
      content: ["Text"],
    },
  ];

  const paths = [
    {
      name: "Term Rewriting",
      path: "/rewriting/introduction",
    },
    {
      name: "Ressources",
      path: "/rewriting/ressources",
    },
    {
      name: "External ressources",
      path: "/rewriting/ressources/external-ressources",
    },
  ];

  return (
    <div className="container">
      <NavBar theme="rewriting" />
      <div className="main">
        <Path paths={paths} branch="rewriting" />
        <Title title="External Ressources" />
        {sections.map((section, index) => (
          <Section
            key={index}
            title={section.title}
            content={section.content}
          />
        ))}
      </div>
      <Footer />
    </div>
  );
};

export default ExternalRessourcesPage;

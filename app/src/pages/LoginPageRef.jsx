import React, { useContext, useEffect, useState } from "react";
import Title from "../components/Title";
import Footer from "../components/Footer";
import TextField from "@mui/material/TextField";
import { Box, Button } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../UserContext";
import { IoArrowBackOutline } from "react-icons/io5";
import API_URL from "../constants";

const LoginPageRef = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [redirect, setRedirect] = useState(false);
  const [error, setError] = useState(false);
  const { setUserInfo } = useContext(UserContext);
  const navigate = useNavigate();

  useEffect(() => {
    localStorage.getItem("authenticated") && setRedirect(true);
  }, []);

  if (redirect) {
    navigate("/rewriting/intranet");
  }

  async function login(ev) {
    ev.preventDefault();
    const response = await fetch(API_URL + "login", {
      method: "POST",
      body: JSON.stringify({ username, password }),
      headers: { "Content-Type": "application/json" },
      credentials: "include",
    });
    if (response.ok) {
      response.json().then((userInfo) => {
        localStorage.setItem("authenticated", true);
        setUserInfo(userInfo);
        setRedirect(true);
      });
    } else {
      setError(true);
    }
  }

  return (
    <div className="login-page">
      <div className="login-card">
        <Title title="Intranet" />
        <form
          className="register"
          onSubmit={login}
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            gap: "1rem",
            width: "100%",
          }}
        >
          {error ? (
            <TextField
              margin="normal"
              fullWidth
              error
              helperText="Incorrect entry."
              name="Email"
              label="Email"
              type="email"
              id="email"
              autoComplete="current-email"
              maxRows={1}
              onChange={(e) => setUsername(e.target.value)}
            />
          ) : (
            <TextField
              margin="normal"
              required
              fullWidth
              name="username"
              label="Email"
              type="email"
              id="email"
              autoComplete="current-email"
              maxRows={1}
              onChange={(e) => setUsername(e.target.value)}
            />
          )}
          {error ? (
            <TextField
              margin="normal"
              error
              helperText="Incorrect entry."
              fullWidth
              name="Password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              maxRows={1}
              onChange={(e) => setPassword(e.target.value)}
            />
          ) : (
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              maxRows={1}
              onChange={(e) => setPassword(e.target.value)}
            />
          )}
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Login
          </Button>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "flex-start",
              width: "100%",
            }}
          >
            <Button
              onClick={() => navigate("/rewriting")}
              fullWidth
              variant="contained"
              sx={{ minWidth: "200px" }}
            >
              <IoArrowBackOutline size={30} />
              Return to Home
            </Button>
          </Box>
        </form>
      </div>
      <Footer />
    </div>
  );
};

export default LoginPageRef;

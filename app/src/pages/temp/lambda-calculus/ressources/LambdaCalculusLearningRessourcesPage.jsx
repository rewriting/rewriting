import React, { useEffect } from "react";
import NavBar from "../../../../components/NavBar";
import Title from "../../../../components/Title";
import Path from "../../../../components/Path";
import { NavLink } from "react-router-dom";
import Ressource from "../../../../components/Ressource";
import Footer from "../../../../components/Footer";

const LambdaCalculusLearningRessourcesPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  const links = [
    {
      path: "/rewriting/temp/lambda-calculus/ressources/learning-ressources/books",
      title: "Books",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odio, earum.",
    },
    {
      path: "/rewriting/temp/lambda-calculus/ressources/learning-ressources/surveys",
      title: "Surveys",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odio, earum.",
    },
    {
      path: "/rewriting/temp/lambda-calculus/ressources/learning-ressources/courses",
      title: "Courses",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odio, earum.",
    },
  ];

  const paths = [
    {
      name: "Home",
      path: "/rewriting/temp",
    },
    {
      name: "Lambda Calculus",
      path: "/rewriting/temp/lambda-calculus/introduction",
    },
    {
      name: "Ressources",
      path: "/rewriting/temp/lambda-calculus/ressources",
    },
    {
      name: "Learning ressources",
      path: "/rewriting/temp/lambda-calculus/ressources/learning-ressources",
    },
  ];

  return (
    <div className="container">
      <NavBar theme="lambda-calculus" />
      <div className="main">
        <Path paths={paths} branch="lambda-calculus" />
        <Title title="Learning Ressources" />
        <div className="ressources-cards-container">
          {links.map(({ path, title, description }) => (
            <NavLink key={title} to={path}>
              <Ressource ressourceTitle={title} description={description} />
            </NavLink>
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default LambdaCalculusLearningRessourcesPage;

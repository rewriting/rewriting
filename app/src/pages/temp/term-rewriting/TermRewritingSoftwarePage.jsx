// import React, { useEffect } from "react";
// import NavBar from "../../components/NavBar";
// import Title from "../../components/Title";
// import Section from "../../components/Section";
// import Path from "../../components/Path";
// import Footer from "../../components/Footer";
// import { TempTermRewritingPath } from "../../components/temp/TempTermRewritingPath";

// const TermRewritingSoftwarePage = () => {
//   useEffect(() => {
//     window.scrollTo(0, 0);
//   }, []);
//   const sections = [
//     {
//       title: "AProVE",
//       content: [
//         "A tool for automated termination and innermost termination proofs of (conditional) term rewrite systems (TRSs). AProVE offers the techniques of polynomial orders, recursive path orders possibly with status, dependency pairs including recent refinements such as narrowing, rewriting, and (forward-)instantiation of dependency pairs, and the size-change principle, also in combination with dependency pairs. The tool is written in Java and proofs can be performed both in a fully automated or in an interactive mode via a graphical user interface.",
//         "Contact: AProVE Group <aprove @ i2.informatik.rwth-aachen.de>",
//       ],
//     },
//     {
//       title: "CoLoR ",
//       content: [
//         "a Coq Library on Rewriting and termination.",
//         "Coq is a proof assistant developped at INRIA available at http://coq.inria.fr/. The aim of this Coq library is to provide the necessary formal basis for certifying the termination witnesses searched and built by termination tools like CiME, AProVE, TTT, Cariboo, etc.",
//       ],
//     },
//     {
//       title: "Maude",
//       content: [
//         "a high-performance multiparadigm language based on rewriting logic, and contains a functional sublanguage based on equational logic.",
//         "It can be used for both programming and executable system specification in a variety of areas such as distributed and mobile systems and communication protocols, and functional applications. Thanks to its reflective features it can be used as a metalanguage and is easily extensible with powerful module composition operations as those supported in its Full Maude extension. It can also be used as a semantic framework to specify and prototype different languages, concurrency calculi, and SOS specifications; and as a logical framework to represent and mechanize different logics and proving tools.",
//       ],
//     },
//     {
//       title: "Section",
//       content: ["Text"],
//     },
//   ];

//   //Paths for the path bar
//   const paths = [
//     {
//       name: "Home",
//       path: "/rewriting/temp",
//     },
//     {
//       name: "Term Rewriting",
//       path: "/rewriting/temp/term-rewriting/introduction",
//     },
//     {
//       name: "Software",
//       path: "/rewriting/temp/term-rewriting/software",
//     },
//   ];

//   return (
//     <div className="container">
//       <NavBar theme="rewriting" paths={<TempTermRewritingPath />} />
//       <div className="main">
//         <Path paths={paths} branch="rewriting" />
//         <Title title="Software" />
//         {sections.map((section, index) => (
//           <Section
//             key={index}
//             title={section.title}
//             content={section.content}
//           />
//         ))}
//       </div>
//       <Footer />
//     </div>
//   );
// };

// export default TermRewritingSoftwarePage;

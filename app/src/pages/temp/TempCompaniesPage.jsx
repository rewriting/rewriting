import React, { useEffect } from "react";
import Title from "../../components/Title";
import Card from "../../components/Card";
import Path from "../../components/Path";
import Footer from "../../components/Footer";

const TempCompaniesPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const links = [
    {
      title: "Company n°1",
      url: "",
      tags: ["Rewriting"],
    },
    {
      title: "Company n°2",
      url: "",
      tags: ["Lambda Calculus"],
    },
    {
      title: "Company n°3",
      url: "",
      tags: ["Logic"],
    },
    {
      title: "Company n°4",
      url: "",
      tags: ["Rewriting", "Logic", "Lambda Calculus"],
    },
    {
      title: "Company n°5",
      url: "",
      tags: ["Rewriting"],
    },
    {
      title: "Company n°6",
      url: "",
      tags: ["Rewriting"],
    },
    {
      title: "Company n°7",
      url: "",
      tags: ["Rewriting", "Logic", "Lambda Calculus"],
    },
    {
      title: "Company n°8",
      url: "",
      tags: ["Logic"],
    },
  ];

  const paths = [
    {
      name: "Home",
      path: "/rewriting/temp",
    },
    {
      name: "Companies",
      path: "/rewriting/temp/companies",
    },
  ];

  return (
    <div className="container">
      <div className="main no-nav-bar">
        <Path paths={paths} />
        <Title title="Companies" />
        <div className="industrial-applications-cards-container">
          {links.map(({ title, url, tags }) => (
            <Card title={title} url={url} tags={tags} />
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default TempCompaniesPage;

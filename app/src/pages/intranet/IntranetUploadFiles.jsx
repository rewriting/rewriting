import React, { useEffect, useState } from "react";
import Title from "../../components/Title";
import NavBar from "../../components/NavBar";
import Footer from "../../components/Footer";
import Axios from "axios";
import { MdDownload } from "react-icons/md";
import { Alert } from "@mui/material";

const IntranetUploadFiles = () => {
  const [isSelected, setIsSelected] = useState(false);
  const [file, setFile] = useState();
  // const [data, setData] = useState([]);
  const [filesCodes, setFilesCodes] = useState([]);
  const [haveFiles, setHaveFiles] = useState(false);
  const [fileSuccessfulyUploaded, setFileSuccessfulyUploaded] = useState(false);

  useEffect(() => {
    // const fetchData = async () => {
    //   try {
    //     const response = await Axios.get("https://rewriting.inria.fr/api/files");
    //     setData(response.data);
    //   } catch (error) {
    //     console.log(error);
    //   }
    // };

    const fetchFilesCodes = async () => {
      try {
        const response = await Axios.get(
          "https://rewriting.inria.fr/api/filescodes"
        );
        setFilesCodes(response.data);
      } catch (error) {
        console.log(error);
      }
    };

    if (fileSuccessfulyUploaded) {
      fetchFilesCodes();
    }

    if (!haveFiles) {
      // fetchData();
      fetchFilesCodes();
      setHaveFiles(true);
    }
  }, [fileSuccessfulyUploaded, haveFiles]);
  const handleFile = (event) => {
    setFile(event.target.files[0]);
    console.log(event.target.files[0]);
    setIsSelected(true);
  };

  function handleUpload() {
    if (!(file instanceof File) || file.size === 0) {
      console.error("Error reading file");
      return;
    }
    const formData = new FormData();
    formData.append("file", file);
    fetch("https://rewriting.inria.fr/api/files", {
      method: "POST",
      body: formData,
    })
      .then((response) => response.json())
      .then((result) => {
        setFileSuccessfulyUploaded(true);
        document.getElementById("file").value = "";
      })
      .catch((error) => {
        console.error("error", error);
      });
  }

  function bytesToMB(bytes) {
    return bytes / 1000000 + " MB";
  }

  return (
    <div className="container">
      <NavBar
        theme="intranet"
        paths={[
          {
            id: 1,
            name: "Home",
            path: "/rewriting/temp/intranet",
          },
          {
            id: 2,
            name: "Upload files",
            path: "/rewriting/temp/intranet/upload-files",
          },
          {
            id: 3,
            name: "Account",
            path: "/rewriting/temp/intranet/account",
          },
        ]}
      />
      {fileSuccessfulyUploaded && (
        <div className="alert fade-out">
          <Alert className="alert" severity="success">
            File uploaded successfully!
          </Alert>
        </div>
      )}
      <div className="main upload-page">
        <div className="upload-form">
          <Title title="Upload your files" />
          {/* <form encType="multipart/form-data"> */}
          <input type="file" name="file" id="" onChange={handleFile} />
          {isSelected ? (
            <div>
              <p>Filename: {file.name}</p>
              <p>Filetype: {file.type}</p>
              <p>Size in bytes: {file.size}</p>
              <p>
                lastModifiedDate: {file.lastModifiedDate.toLocaleDateString()}
              </p>
            </div>
          ) : (
            <p>Select a file</p>
          )}
          <div>
            <button type="submit" onClick={handleUpload}>
              Upload
            </button>
          </div>
        </div>
        <div className="uploaded-files-container">
          {filesCodes
            .slice(0)
            .reverse()
            .map((file, index) => (
              <a
                className="downloadable-file"
                key={index}
                href={
                  "https://rewriting.inria.fr/api/files/" +
                  file.filename +
                  "/" +
                  file.originalname
                }
                download
              >
                <p>{file.originalname}</p>
                <p>{bytesToMB(file.size)}</p>
                <MdDownload size={20} />

                {/* <p>{bytesToMB(file.size)}MB</p> */}
              </a>
            ))}
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default IntranetUploadFiles;

const mongoose = require("mongoose");
const { Schema, model } = mongoose;

const TextSchema = new Schema(
  {
    name: String,
    content: String,
  },
  {
    timestamps: true,
  }
);

const AddTextSchemaModel = model("Text", TextSchema);

module.exports = AddTextSchemaModel;

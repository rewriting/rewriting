const mongoose = require("mongoose");
const { Schema, model } = mongoose;

const OpenProblemSchema = new Schema(
  {
    entitled: String,
    status: String,
    text: String,
    tags: [String],
  },
  {
    timestamps: true,
  }
);

const OpenProblemSchemaModel = model("OpenProblem", OpenProblemSchema);

module.exports = OpenProblemSchemaModel;

const mongoose = require("mongoose");
const { Schema, model } = mongoose;

const WorkshopSchema = new Schema(
  {
    name: String,
    description: String,
    website: String,
    tags: [String],
    past: [{ name: String, url: String }],
    upcoming: [{ name: String, url: String }],
  },
  {
    timestamps: true,
  }
);

const WorkshopSchemaModel = model("Workshop", WorkshopSchema);

module.exports = WorkshopSchemaModel;

const mongoose = require("mongoose");
const { Schema, model } = mongoose;

const ConferenceSchema = new Schema(
  {
    name: String,
    description: String,
    website: String,
    tags: [String],
    past: [{ name: String, url: String }],
    upcoming: [{ name: String, url: String }],
  },
  {
    timestamps: true,
  }
);

const ConferenceSchemaModel = model("Conference", ConferenceSchema);

module.exports = ConferenceSchemaModel;

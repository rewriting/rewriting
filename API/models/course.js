const mongoose = require("mongoose");
const { Schema, model } = mongoose;

const CourseSchema = new Schema(
  {
    name: String,
    description: String,
    link: String,
    tags: [String],
  },
  {
    timestamps: true,
  }
);

const AddCourseSchemaModel = model("Course", CourseSchema);

module.exports = AddCourseSchemaModel;

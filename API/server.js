//imports
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const multer = require("multer");
const upload = multer({ dest: "./public/data/uploads/" });
const path = require("path");
const app = express();

function logInfo(action, message) {
  console.log("\x1b[36m%s\x1b[0m", "[INFO] - " + action);
  console.log(message + "\n");
}
// const ORIGIN_URL = "http://localhost:3000";
const ORIGIN_URL = "https://rewriting.gitlabpages.inria.fr";

//cors & body-parser
app.use(
  cors({
    origin: ORIGIN_URL,
  }),
  bodyParser.urlencoded({ extended: true }),
  (req, res, next) => {
    res.setHeader(
      "Content-Security-Policy",
      "default-src 'none'; font-src 'self'"
    );
    next();
  }
);

// for parsing application/json
app.use(express.json());

const users = [
  {
    email: "test1@gmail.com",
    password: "mdp",
  },
  {
    email: "test2@gmail.com",
    password: "mdp",
  },
  {
    email: "test3@gmail.com",
    password: "mdp",
  },
];

// Route "/api/users"
app.get("/api/users", (req, res) => {
  res.send(users);
});

//login (emails & hashed passwords)
const loginItems = [];

//register
app.post("/api/login", (req, res) => {
  const loginItem = req.body;
  loginItems.push(loginItem);
  res.send(loginItems);
});

//login
app.get("/api/login", (req, res) => {
  res.send(loginItems);
});

// //files list
// const filesItems = [];
// const uploadedFiles = [];

// //upload files
// app.post("/api/files", upload.single("file"), function (req, res, next) {
//   uploadedFiles.push({
//     originalname: req.file.originalname,
//     filename: req.file.filename,
//     size: req.file.size,
//   });
//   filesItems.push(req.file);
//   res.send(filesItems);

//   //Logs
//   const date = new Date();
//   const message =
//     JSON.stringify(req.file) + "\nDatetime : " + date + "\nSender : <EMAIL>";
//   logInfo("UPLOAD", message);
// });

// //get files
// app.get("/api/files", (req, res) => {
//   res.send(filesItems);
// });

// //download the file
// app.get("/api/files/:filename/:originalname", (req, res) => {
//   const filename = req.params.filename;
//   const filePath = path.join(__dirname, "public", "data", "uploads", filename);

//   // Specify the new file name
//   const newFilename = req.params.originalname;

//   // Set the appropriate headers for the download
//   res.setHeader("Content-Disposition", `attachment; filename="${newFilename}"`);
//   res.setHeader("Content-Type", "application/octet-stream");

//   res.sendFile(filePath);

//   const json = {
//     filename: filename,
//     filepath: filePath,
//     originalname: newFilename,
//   };
//   //Logs
//   const date = new Date();
//   const message =
//     JSON.stringify(json) + "\nDatetime : " + date + "\nReceiver : <EMAIL>";
//   logInfo("DOWNLOAD", message);
// });

// //get files codes
// app.get("/api/filescodes", (req, res) => {
//   res.send(uploadedFiles);
// });

// Redirection for route "/"
app.get("/", (req, res, next) => {
  res.redirect("http://localhost:3000/rewriting/");
});

const port = 5000;
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
